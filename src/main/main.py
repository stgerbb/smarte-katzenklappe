import RPi.GPIO as GPIO
from gpiozero import MotionSensor
from signal import pause
import subprocess
import time

STEPPER_PIN1 = 12
STEPPER_PIN2 = 16
STEPPER_PIN3 = 20
STEPPER_PIN4 = 21

STEPPER_DELAY = 0.005

PIR = MotionSensor(15)

def setup():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(STEPPER_PIN1, GPIO.OUT)
    GPIO.setup(STEPPER_PIN2, GPIO.OUT)
    GPIO.setup(STEPPER_PIN3, GPIO.OUT)
    GPIO.setup(STEPPER_PIN4, GPIO.OUT)
    
# Für forward_step und backward_step wird die Sequenz 
# für eine hohe Geschwindigkeit aber weniger Drehmoment verwendet
def forward_step():
    set_stepper(1, 0, 0, 0)
    set_stepper(0, 1, 0, 0)
    set_stepper(0, 0, 1, 0)
    set_stepper(0, 0, 0, 1)
    
def backward_step():
    set_stepper(0, 0, 0, 1)
    set_stepper(0, 0, 1, 0)
    set_stepper(0, 1, 0, 0)
    set_stepper(1, 0, 0, 0)
  
def set_stepper(i1, i2, i3, i4):
    GPIO.output(STEPPER_PIN1, i1)
    GPIO.output(STEPPER_PIN2, i2)
    GPIO.output(STEPPER_PIN3, i3)
    GPIO.output(STEPPER_PIN4, i4)
    time.sleep(STEPPER_DELAY)

def open_and_close_flap():
    # 512 Schritte werden für eine 360° Umdrehung benötigt
    print(50*"#")
    print()
    print(">> Katzenklappe wird geöffnet.")
    
    for i in range(1664): # = 3 1/4 Umdrehungen
        forward_step()
    
    print(">> Warte drei Sekunden...")
    time.sleep(3)
    
    print(">> Katzenklappe wird geschlossen.")
    
    for i in range(1664): # = 3 1/4 Umdrehungen
        backward_step()
    
    print()
    print(50*"#")
    

def no_motion():
    print()
    print(">> Warte auf eine Bewegung...")
   
def detect_object():
    print_motion_detected()
    
    proc = subprocess.Popen(['ssh', 'root@10.0.0.3', 'DISPLAY=:1 python3 /root/Desktop/object_detection/run_object_detector_cat.py'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)

    try:
        output, errors = proc.communicate()
        print(output)
        print(errors)
        open_and_close_flap()
    except Exception:
        proc.kill()
        output, errors = proc.communicate()
        print(output)
        print(errors)

def print_startup():
    print(50*"#")
    print()
    print(">> main.py wurde gestartet.")
    print(">> Warte auf Bewegung...")
    print()
    print(50*"#")

def print_motion_detected():
    print()
    print(">> Eine Bewegung wurde erkannt.")
    print(">> Objekterkennung wird gestartet...")
    print()
    print(50*"#")



setup()

try:
    print_startup()
    PIR.when_motion = detect_object
    PIR.when_no_motion = no_motion
    pause()
except KeyboardInterrupt:
    print(">> Beende main.py")
    print()
    print(50*"#")
finally:
    print()
    print(">> main.py wird beendet!")
    exit()