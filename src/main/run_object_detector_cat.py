import numpy as np
import os
import sys
import six.moves.urllib as urllib
import tarfile
import tensorflow as tf
import zipfile
import logging

from distutils.version import StrictVersion
from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image

import cv2

from utils import ops as utils_ops
from utils import label_map_util
from utils import visualization_utils as vis_util

logging.basicConfig(level=logging.INFO, format='>> %(levelname)s: %(message)s')

if StrictVersion(tf.__version__) < StrictVersion('1.9.0'):
  raise ImportError('Please upgrade your TensorFlow installation to v1.9.* or later!')

MODEL_NAME = 'ssdlite_mobilenet_v2_coco_2018_05_09'
MODEL_FILE = MODEL_NAME + '.tar.gz'
DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'

# Absoluter Pfad zu den Dateien ändern
PATH_TO_FROZEN_GRAPH = '/root/Desktop/object_detection/ssdlite_mobilenet_v2_coco_2018_05_09/frozen_inference_graph.pb'
PATH_TO_LABELS = '/root/Desktop/object_detection/data/mscoco_label_map.pbtxt'

# Muss nur ausgeführt werden, wenn das Model nicht vorhanden ist und heruntergeladen werden muss.
#opener = urllib.request.URLopener()
#opener.retrieve(DOWNLOAD_BASE + MODEL_FILE, MODEL_FILE)
#tar_file = tarfile.open(MODEL_FILE)
#for file in tar_file.getmembers():
#  file_name = os.path.basename(file.name)
#  if 'frozen_inference_graph.pb' in file_name:
#    tar_file.extract(file, os.getcwd())

detection_graph = tf.Graph()
with detection_graph.as_default():
  od_graph_def = tf.compat.v1.GraphDef()
  with tf.io.gfile.GFile(PATH_TO_FROZEN_GRAPH, 'rb') as fid:
    serialized_graph = fid.read()
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')

category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS, use_display_name=True)

logging.info('Initialisierung der Objekterkennung erfolgreich!')

def run_inference_for_single_image(image, graph):
    if 'detection_masks' in tensor_dict:
        detection_boxes = tf.squeeze(tensor_dict['detection_boxes'], [0])
        detection_masks = tf.squeeze(tensor_dict['detection_masks'], [0])
        real_num_detection = tf.cast(tensor_dict['num_detections'][0], tf.int32)
        detection_boxes = tf.slice(detection_boxes, [0, 0], [real_num_detection, -1])
        detection_masks = tf.slice(detection_masks, [0, 0, 0], [real_num_detection, -1, -1])
        detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(detection_masks, detection_boxes, image.shape[0], image.shape[1])
        detection_masks_reframed = tf.cast(tf.greater(detection_masks_reframed, 0.5), tf.uint8)
        tensor_dict['detection_masks'] = tf.expand_dims(detection_masks_reframed, 0)
    
    image_tensor = tf.compat.v1.get_default_graph().get_tensor_by_name('image_tensor:0')

    output_dict = sess.run(tensor_dict, feed_dict={image_tensor: np.expand_dims(image, 0)})
    output_dict['num_detections'] = int(output_dict['num_detections'][0])
    output_dict['detection_classes'] = output_dict['detection_classes'][0].astype(np.uint8)
    output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
    output_dict['detection_scores'] = output_dict['detection_scores'][0]
    
    if 'detection_masks' in output_dict:
        output_dict['detection_masks'] = output_dict['detection_masks'][0]
    return output_dict

logging.info('USB Webcam wird gestartet...')

cap = cv2.VideoCapture(0)

frame_rate_calc = 1
freq = cv2.getTickFrequency()
font = cv2.FONT_HERSHEY_SIMPLEX

# Start Objekterkennung
try:
    with detection_graph.as_default():
        with tf.compat.v1.Session() as sess:
                ops = tf.compat.v1.get_default_graph().get_operations()
                all_tensor_names = {output.name for op in ops for output in op.outputs}
                tensor_dict = {}
                for key in [
                  'num_detections', 'detection_boxes', 'detection_scores',
                  'detection_classes', 'detection_masks'
                ]:
                    tensor_name = key + ':0'
                    if tensor_name in all_tensor_names:
                        tensor_dict[key] = tf.compat.v1.get_default_graph().get_tensor_by_name(tensor_name)
                        
                logging.info('USB Webcam wurde erfolgreich gestartet!')

                cat_counter = 0                

                while True:
                    t1 = cv2.getTickCount()
                    
                    ret, image_np = cap.read()
                    
                    image_np_expanded = np.expand_dims(image_np, axis=0)
                    output_dict = run_inference_for_single_image(image_np, detection_graph)
                    vis_util.visualize_boxes_and_labels_on_image_array(
                        image_np,
                        output_dict['detection_boxes'],
                        output_dict['detection_classes'],
                        output_dict['detection_scores'],
                        category_index,
                        instance_masks=output_dict.get('detection_masks'),
                        use_normalized_coordinates=True,
                        line_thickness=8,
			min_score_thresh=0.75)
                    
                    cv2.putText(image_np,"FPS: {0:.2f}".format(frame_rate_calc),(30,50),font,1,(255,255,0),2,cv2.LINE_AA)
                    cv2.imshow('Object Detector', cv2.resize(image_np, (720, 480)))
                    
                    t2 = cv2.getTickCount()
                    time1 = (t2-t1)/freq
                    frame_rate_calc = 1/time1

                    # Erkennung Katze mit Abbruchbedingung
                    for i in range(len(output_dict['detection_boxes'])):
                        class_name = category_index[output_dict['detection_classes'][i]]['name']
                        if class_name == 'cat' and output_dict['detection_scores'][i] >= 0.75:
                            logging.info('Katze | ' + str(output_dict['detection_scores'][i]))
                            cat_counter += 1
                            
                    if cat_counter == 20:
                        logging.info('Eine Katze wurde zu mehr als 75% erkannt. Öffne Katzenklappe...')
                        cap.release()
                        cv2.destroyAllWindows()
                        break                    

                    if cv2.waitKey(25) & 0xFF == ord('q'):
                        cap.release()
                        cv2.destroyAllWindows()
                        break
except Exception as e:
    print(e)
    cap.release()