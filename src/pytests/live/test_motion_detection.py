import detect_motion as dm

def test_setup():
    assert str(dm.pir.pin) == 'GPIO15'
    assert dm.pir.value == 0
    assert dm.pir.motion_detected == False

def test_motion_detected():
    dm.when_motion()
    while (dm.pir.value != 1):
        pass
    assert dm.pir.value == 1
    assert dm.pir.motion_detected == True
    assert dm.mo == True
        
def test_no_motion_detected():
    dm.when_no_motion()
    while (dm.pir.value != 0):
        pass
    assert dm.pir.value == 0
    assert dm.pir.motion_detected == False
    assert dm.no_mo == True