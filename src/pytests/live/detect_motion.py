from gpiozero import MotionSensor

pir = MotionSensor(15)
mo = False
no_mo = False

def motion():
    mo = True
    
def no_motion():
    no_mo = True

def when_motion():
    pir.when_motion = motion
    
def when_no_motion():
    pir.when_no_motion = no_motion