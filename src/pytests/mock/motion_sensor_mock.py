from time import sleep

class MotionSensor:
    device_active = False
    value = 0
    threshold = 0
    
    def __init__(self, pin):
        self.pin = pin
    
    def wait_for_motion(self, timeout=None):
        if not timeout == None:
            sleep(timeout)
        while(self.threshold < 100):
            self.threshold += 1
        self.device_active = True        
        if self.device_active:
            self.when_motion()
        
    def wait_for_no_motion(self, timeout=None):
        if not timeout == None:
            sleep(timeout)
        self.device_active = False
        if not self.device_active:
            self.when_no_motion()
        
    def motion_detected(self):
        self.value = 1
        self.threshold = 0
    
    def close(self):
        self.pin = None
        self.device_active = False
        self.value = 0
        self.threshold = 0
    
    def when_motion(self):
        if not self.device_active:
            while(self.threshold < 100):
                self.threshold += 1
            self.device_active = True
        if self.device_active:
            self.motion_detected()
    
    def when_no_motion(self):
        self.value = 0
        self.threshold = 0