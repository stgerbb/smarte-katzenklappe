from motion_sensor_mock import MotionSensor

pir = MotionSensor(15)

def test_setup():
    assert pir.pin == 15
    assert pir.value == 0
    assert pir.device_active == False

def test_wait_for_motion_without_timeout():
    pir.wait_for_motion()
    assert pir.device_active == True
    assert pir.value == 1
    assert pir.threshold == 0
    
def test_wait_for_motion_wit_timeout():
    pir.wait_for_motion(5)
    assert pir.device_active == True
    assert pir.value == 1
    assert pir.threshold == 0
    
def test_motion_detected():
    pir.when_motion()
    assert pir.device_active == True
    assert pir.value == 1
    assert pir.threshold == 0
    
def test_wait_for_no_motion_without_timeout():
    pir.wait_for_no_motion()
    assert pir.device_active == False
    assert pir.value == 0
    assert pir.threshold == 0
    
def test_wait_for_no_motion_with_timeout():
    pir.wait_for_no_motion(5)
    assert pir.device_active == False
    assert pir.value == 0
    assert pir.threshold == 0

def test_close():
    pir.close()
    assert pir.device_active == False
    assert pir.value == 0
    assert pir.threshold == 0
    assert pir.pin == None